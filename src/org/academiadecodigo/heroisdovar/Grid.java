package org.academiadecodigo.heroisdovar;

public class Grid {
    public static int COLS = 50;
    public static int ROWS = 50;

    public static final int PADDING = 10;
    public static int RECTANGLE_SIZE = 20;
}
