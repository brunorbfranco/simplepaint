package org.academiadecodigo.heroisdovar;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

public class InputHadler implements KeyboardHandler {

    private Brush brush;

    private GridGenerator gridGenerator;
    private Keyboard keyboard;
    private KeyboardEvent[] keyPresses;

    public InputHadler(Brush brush, GridGenerator gridGenerator) {
        this.brush = brush;
        this.gridGenerator = gridGenerator;
        keyboard = new Keyboard(this);
        createKeyPressEvents();
    }

    public void createKeyPressEvents() {
        keyPresses = new KeyboardEvent[12];

        for (int i = 0; i < keyPresses.length; i++) {
            keyPresses[i] = new KeyboardEvent();
        }

        keyPresses[0].setKey(KeyboardEvent.KEY_SPACE);
        keyPresses[1].setKey(KeyboardEvent.KEY_UP);
        keyPresses[2].setKey(KeyboardEvent.KEY_DOWN);
        keyPresses[3].setKey(KeyboardEvent.KEY_LEFT);
        keyPresses[4].setKey(KeyboardEvent.KEY_RIGHT);
        keyPresses[5].setKey(KeyboardEvent.KEY_S);
        keyPresses[6].setKey(KeyboardEvent.KEY_L);
        keyPresses[7].setKey(KeyboardEvent.KEY_C);
        keyPresses[8].setKey(KeyboardEvent.KEY_1);
        keyPresses[9].setKey(KeyboardEvent.KEY_2);
        keyPresses[10].setKey(KeyboardEvent.KEY_3);
        keyPresses[11].setKey(KeyboardEvent.KEY_4);

        for (int i = 0; i < keyPresses.length; i++) {
            keyPresses[i].setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
            keyboard.addEventListener(keyPresses[i]);
        }
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {

        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_LEFT:
                if (!(brush.getX() - Grid.RECTANGLE_SIZE < Grid.PADDING)) {
                    if (brush.isInPaintMode() || brush.isInDeleteMode()) brush.paintOrDelete();
                    brush.translate(-Grid.RECTANGLE_SIZE, 0);
                }
                break;
            case KeyboardEvent.KEY_UP:
                if (!(brush.getY() - Grid.RECTANGLE_SIZE < Grid.PADDING)) {
                    if (brush.isInPaintMode() || brush.isInDeleteMode()) brush.paintOrDelete();
                    brush.translate(0, -Grid.RECTANGLE_SIZE);
                }
                break;
        case KeyboardEvent.KEY_RIGHT:
                if (!(brush.getX() + brush.getWidth() + Grid.RECTANGLE_SIZE > Grid.PADDING + Grid.COLS * Grid.RECTANGLE_SIZE)) {
                    if (brush.isInPaintMode() || brush.isInDeleteMode()) brush.paintOrDelete();
                    brush.translate(Grid.RECTANGLE_SIZE, 0);
                }
            break;
            case KeyboardEvent.KEY_DOWN:
                if (!(brush.getY() + brush.getHeight() + Grid.RECTANGLE_SIZE > Grid.PADDING + Grid.ROWS * Grid.RECTANGLE_SIZE)) {
                    if (brush.isInPaintMode() || brush.isInDeleteMode()) brush.paintOrDelete();
                    brush.translate(0, Grid.RECTANGLE_SIZE);
                }
                break;
            case KeyboardEvent.KEY_SPACE:
                if (brush.isInNormalMode()) {
                    brush.setColor(Color.YELLOW);
                    brush.setInNormalMode(false);
                    brush.setInPaintMode(true);
                    brush.setInDeleteMode(false);
                } else
                if (brush.isInPaintMode()){
                    brush.setColor(Color.RED);
                    brush.setInNormalMode(false);
                    brush.setInPaintMode(false);
                    brush.setInDeleteMode(true);
                } else
                if (brush.isInDeleteMode()){
                    brush.setColor(Color.GREEN);
                    brush.setInNormalMode(true);
                    brush.setInPaintMode(false);
                    brush.setInDeleteMode(false);
                }
                break;
            case KeyboardEvent.KEY_S:
                gridGenerator.save();
                break;
            case KeyboardEvent.KEY_L:
                gridGenerator.delete();
                gridGenerator.load();
                gridGenerator.draw();
                brush.delete();
                brush.fill();
                break;
            case KeyboardEvent.KEY_C:
                gridGenerator.delete();
                brush.delete();
                brush.fill();
            case KeyboardEvent.KEY_1:
                brush.setPaintingRed(true);
                brush.setPaintingGreen(false);
                brush.setPaintingBlue(false);
                brush.setPaintingRandom(false);
                break;
            case KeyboardEvent.KEY_2:
                brush.setPaintingRed(false);
                brush.setPaintingGreen(true);
                brush.setPaintingBlue(false);
                brush.setPaintingRandom(false);
                break;
            case KeyboardEvent.KEY_3:
                brush.setPaintingRed(false);
                brush.setPaintingGreen(false);
                brush.setPaintingBlue(true);
                brush.setPaintingRandom(false);
                break;
            case KeyboardEvent.KEY_4:
                brush.setPaintingRed(false);
                brush.setPaintingGreen(false);
                brush.setPaintingBlue(false);
                brush.setPaintingRandom(true);
                break;
        }

    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

    }
}
