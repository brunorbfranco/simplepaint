package org.academiadecodigo.heroisdovar;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

import java.io.*;
import java.util.Random;

public class GridGenerator {
    private MapTile[] rectangles;
    private static MapTile[][] map;
    private int[][] rectangleNumber;
    private Random random = new Random();

    public GridGenerator() {
        rectangles = new MapTile[6];
        map = new MapTile[Grid.COLS][Grid.ROWS];
        rectangleNumber = new int[Grid.COLS][Grid.ROWS];
    }

    public static MapTile[][] getMap() {
        return map;
    }


    public void load() {
        try {
            InputStream is = new FileInputStream("src/org/academiadecodigo/heroisdovar/drawing.txt");
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            int col = 0;
            int row = 0;

            System.out.println("--------Start load loop----------");

            while (col < Grid.COLS && row < Grid.ROWS) {
                String line = br.readLine();

                while (col < Grid.COLS) {

                    String[] numbers = line.split("");

                    int num = Integer.parseInt(numbers[col]);

                    rectangleNumber[col][row] = num;

                    if (num == 0) {
                        map[col][row] = new MapTile(col * Grid.RECTANGLE_SIZE + Grid.PADDING, row * Grid.RECTANGLE_SIZE + Grid.PADDING,Grid.RECTANGLE_SIZE,Grid.RECTANGLE_SIZE, Color.BLACK);
                        map[col][row].setIsEmpty(true);
                        map[col][row].setColorNumber(num);
                    }
                    if (num == 1) {
                        map[col][row] = new MapTile(col * Grid.RECTANGLE_SIZE + Grid.PADDING, row * Grid.RECTANGLE_SIZE + Grid.PADDING,Grid.RECTANGLE_SIZE,Grid.RECTANGLE_SIZE, Color.ORANGE);
                        map[col][row].setIsEmpty(false);
                        map[col][row].setColorNumber(num);
                    }
                    if (num == 2) {
                        map[col][row] = new MapTile(col * Grid.RECTANGLE_SIZE + Grid.PADDING, row * Grid.RECTANGLE_SIZE + Grid.PADDING,Grid.RECTANGLE_SIZE,Grid.RECTANGLE_SIZE, Color.RED);
                        map[col][row].setIsEmpty(false);
                        map[col][row].setColorNumber(num);
                    }
                    if (num == 3) {
                        map[col][row] = new MapTile(col * Grid.RECTANGLE_SIZE + Grid.PADDING, row * Grid.RECTANGLE_SIZE + Grid.PADDING,Grid.RECTANGLE_SIZE,Grid.RECTANGLE_SIZE, Color.GREEN);
                        map[col][row].setIsEmpty(false);
                        map[col][row].setColorNumber(num);
                    }
                    if (num == 4) {
                        map[col][row] = new MapTile(col * Grid.RECTANGLE_SIZE + Grid.PADDING, row * Grid.RECTANGLE_SIZE + Grid.PADDING,Grid.RECTANGLE_SIZE,Grid.RECTANGLE_SIZE, Color.BLUE);
                        map[col][row].setIsEmpty(false);
                        map[col][row].setColorNumber(num);
                    }
                    if (num == 5) {
                        map[col][row] = new MapTile(col * Grid.RECTANGLE_SIZE + Grid.PADDING, row * Grid.RECTANGLE_SIZE + Grid.PADDING,Grid.RECTANGLE_SIZE,Grid.RECTANGLE_SIZE, new Color(random.nextInt(256),random.nextInt(256),random.nextInt(256)));
                        map[col][row].setIsEmpty(false);
                        map[col][row].setColorNumber(num);
                    }

                    col++;
                }

                col = 0;
                row++;
            }
            System.out.println("-----End load loop---------");
            br.close();
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public void draw() {

        int col = 0;
        int row = 0;
        int x = Grid.PADDING;
        int y = Grid.PADDING;

        System.out.println("--draw loop start----");
            while (col < Grid.COLS && row < Grid.ROWS) {

            if (map[col][row].getIsEmpty() == true) {
                map[col][row].draw();
            } else {
                map[col][row].fill();
                //map[col][row].setColor(Color.BLACK);
                //map[col][row].draw();
            }
                System.out.println(map[col][row]);

            col++;
            x += Grid.RECTANGLE_SIZE;

            if (col == Grid.COLS) {
                col = 0;
                x = Grid.PADDING;
                row++;
                y += Grid.RECTANGLE_SIZE;
            }
        }
        System.out.println("-----End draw loop--------");
    }

    public void save() {

        String[][] mapInfo = new String[Grid.COLS][Grid.ROWS];

        for (int i = 0; i < mapInfo.length; i++) {
            for (int j = 0; j < mapInfo[i].length; j++) {
                mapInfo[j][i] = Integer.toString(map[i][j].getColorNumber());
            }
        }

        try {

            OutputStream os = new FileOutputStream("src/org/academiadecodigo/heroisdovar/drawing.txt");
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));


            for (String[] row: mapInfo) {
                bw.write(String.join("", row) + "\n");
            }


            bw.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void delete() {
        for (MapTile[] row: map) {
            for (MapTile tile: row) {
                //tile.delete();
                tile.setColor(Color.BLACK);
                tile.setColorNumber(0);
                tile.draw();
            }
        }
    }
}
