package org.academiadecodigo.heroisdovar;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

import java.util.Map;
import java.util.Random;

public class Brush extends Rectangle {

    private boolean inNormalMode;
    private boolean inPaintMode;
    private boolean inDeleteMode;
    private boolean isPaintingRed;
    private boolean isPaintingGreen;
    private boolean isPaintingBlue;
    private boolean isPaintingRandom;
    private Random random = new Random();



    private MapTile[][] map;

    public Brush(int x, int y, int width, int height, Color color) {
        super(x, y, width, height);
        super.setColor(Color.GREEN);
        super.fill();

        this.inNormalMode = true;
        this.inPaintMode = false;
        this.inDeleteMode = false;
        map = GridGenerator.getMap();
    }

    public boolean isInNormalMode() {
        return inNormalMode;
    }

    public boolean isInPaintMode() {
        return inPaintMode;
    }

    public boolean isInDeleteMode() {
        return inDeleteMode;
    }

    public void setInNormalMode(boolean inNormalMode) {
        this.inNormalMode = inNormalMode;
    }
    public void setInPaintMode(boolean inPaintMode) {
        this.inPaintMode = inPaintMode;
    }

    public void setInDeleteMode(boolean inDeleteMode) {
        this.inDeleteMode = inDeleteMode;
    }

    public boolean isPaintingRed() {
        return isPaintingRed;
    }

    public void setPaintingRed(boolean paintingRed) {
        isPaintingRed = paintingRed;
    }

    public boolean isPaintingGreen() {
        return isPaintingGreen;
    }

    public void setPaintingGreen(boolean paintingGreen) {
        isPaintingGreen = paintingGreen;
    }

    public boolean isPaintingBlue() {
        return isPaintingBlue;
    }

    public void setPaintingBlue(boolean paintingBlue) {
        isPaintingBlue = paintingBlue;
    }

    public boolean isPaintingRandom() {
        return isPaintingRandom;
    }

    public void setPaintingRandom(boolean paintingRandom) {
        isPaintingRandom = paintingRandom;
    }
    public void paintOrDelete() {
            int brushLeft = this.getX();
            int brushTop = this.getY();
            int brushRight = this.getX() + this.getWidth();
            int brushBottom = this.getY() + this.getHeight();

            for (MapTile[] col : map) {
                for (MapTile tile : col) {

                    int tileLeft = tile.getX();
                    int tileTop = tile.getY();
                    int tileRight = tile.getX() + tile.getWidth();
                    int tileBottom = tile.getY() + tile.getHeight();

                    if (brushLeft == tileLeft &&
                            brushTop == tileTop &&
                            brushRight == tileRight &&
                            brushBottom == tileBottom) {

                        if (this.isInPaintMode()) {
                            if (this.isPaintingRed()) {
                                tile.setColor(Color.RED);
                                tile.setColorNumber(2);
                                tile.setIsEmpty(false);
                                tile.fill();
                                this.delete();
                                this.fill();
                            }

                            if (this.isPaintingGreen()) {
                                tile.setColor(Color.GREEN);
                                tile.setColorNumber(3);
                                tile.setIsEmpty(false);
                                tile.fill();
                                this.delete();
                                this.fill();
                            }
                            if (this.isPaintingBlue()) {
                                tile.setColor(Color.BLUE);
                                tile.setColorNumber(4);
                                tile.setIsEmpty(false);
                                tile.fill();
                                this.delete();
                                this.fill();
                            }
                            if (this.isPaintingRandom()) {
                                tile.setColor(new Color(random.nextInt(256),random.nextInt(256),random.nextInt(256)));
                                tile.setColorNumber(5);
                                tile.setIsEmpty(false);
                                tile.fill();
                                this.delete();
                                this.fill();
                            }
                        }
                        if (this.isInDeleteMode()) {
                            tile.setColor(Color.BLACK);
                            tile.setIsEmpty(true);
                            tile.setColorNumber(0);
                            tile.draw();
                            this.delete();
                            this.fill();
                        }
                    }
                }


            }
        }


}


