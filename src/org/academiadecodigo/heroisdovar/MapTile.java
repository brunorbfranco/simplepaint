package org.academiadecodigo.heroisdovar;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class MapTile extends Rectangle {

    private boolean isEmpty;
    private int colorNumber;

    public MapTile(int x, int y, int width, int height, Color color) {
        super(x, y, width, height);
        super.setColor(color);
    }

    public boolean getIsEmpty() {
        return isEmpty;
    }

    public int getColorNumber() {
        return colorNumber;
    }

    public void setIsEmpty(boolean isEmpty) {
        this.isEmpty = isEmpty;
    }

    public void setColorNumber(int colorNumber) {
        this.colorNumber = colorNumber;
    }

    public String toString(){
        return super.toString() + colorNumber;
    }
}
