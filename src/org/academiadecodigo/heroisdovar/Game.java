package org.academiadecodigo.heroisdovar;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;

public class Game {

    private GridGenerator gridGenerator;
    private Brush brush;

    public Game() {
        gridGenerator = new GridGenerator();
    }

    public void init() {
        gridGenerator.load();
        gridGenerator.draw();
        brush = new Brush(Grid.PADDING, Grid.PADDING,Grid.RECTANGLE_SIZE,Grid.RECTANGLE_SIZE, Color.GREEN);
        Keyboard keyboard = new Keyboard(new InputHadler(brush, gridGenerator));
    }
}
