Simple Painting application.

Technologies used:
- Java
- Simple Graphics Library

Instructions:
- 1, 2, 3 and 4 for selecting colors:
    - by default no color is selected
    - 1 for Red
    - 2 for Green
    - 3 for Blue
    - 4 for a random color
- Space for changing painting mode:
    - Normal Mode (Green Brush) - Allows to freely move the brush with no alterations (Default Mode)
    - Painting Mode (Yellow Brush) - Paints the tiles with current selected color
- Delete Mode (Red Brush) - Turns the tiles back to white
- Arrows for movement
- S for Saving
- L for loading
- C for clearing the tiles
